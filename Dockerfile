FROM 873345604147.dkr.ecr.sa-east-1.amazonaws.com/sb-python:latest

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

ENV C_FORCE_ROOT=1
ENV PYTHONUNBUFFERED 1
ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

COPY ./omie /usr/src/app/omie
COPY ./main.py /usr/src/app
COPY ./Pipfile /usr/src/app
COPY ./Pipfile.lock /usr/src/app
COPY supervisor.conf /etc/supervisor/conf.d/
COPY ./configuration.json /etc/app/configuration/

RUN apt-get update
RUN apt-get -y install git supervisor vim openssh-client gcc python3-dev \
    libevent-dev libblas-dev libatlas-base-dev \
    libsasl2-dev python-dev libldap2-dev libssl-dev curl \
    libcurl4-openssl-dev jq

RUN pip install -U pip
RUN pip install pycurl
RUN pip install sb-pylogger==1.0.16
RUN set -x \
 && ln -sf $(which python) /bin/python \
 && pip install --upgrade pipenv

RUN pipenv install --system  --deploy

EXPOSE 8080
ENTRYPOINT ["supervisord", "-c", "/etc/supervisor/conf.d/supervisor.conf"]