# Web App Partner ID and KYC

**Overview**<br />
Python Version: ```3.7.3```<br/><br/>
This project will be used to run the IDScore and KycScore apis for customers who did not initially go through this stream.


**Dependencies**<br />
```. SQS AWS```<br />

**HealthCheck**<br />
```. http://localhost:8080/healthcheck```<br />



