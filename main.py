import multiprocessing

from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop

from omie import create_app
from omie.configs import config

if __name__ == '__main__':
    app = create_app()

    server = HTTPServer(app)
    server.bind(config.APP_PORT)
    server.start(multiprocessing.cpu_count() if not config.APP_DEBUG else 1)
    IOLoop.instance().start()
    app.listen(config.APP_PORT)
    IOLoop.current().start()
