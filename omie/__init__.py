from tornado.web import Application

from omie.configs import config
from omie.url import routes


def create_app():
    settings = {"static": "./static", 'debug': config.APP_DEBUG, "cookie_secret": 'abc'}
    app = Application(routes, **settings)
    return app
