import json
import os
import sys
import urllib.parse
from dotenv import load_dotenv
from omie import reports, static
from sb_pylogger.models import Env, Layer
from omie.kyc_and_id import files
import omie

DATA = {}
file_path_prod = '/etc/app/configuration/configuration.json'
if os.path.isfile(file_path_prod):
    with open(file_path_prod, encoding='utf-8') as json_data:
        DATA = json.load(json_data, strict=False)

PATH_ENV_FILE = os.path.join(os.path.dirname(omie.__file__), 'local.env')
if os.path.isfile(PATH_ENV_FILE):
    load_dotenv(PATH_ENV_FILE, verbose=True, override=True)


class Production(object):
    ENVIRONMENT = 'Production'

    LOG_INDEX = 'omie_kyc_id'
    URL_POST_LOG = 'https://prd-sblog.smartbank.net.br'

    API_ENVIRONMENT = Env['PRD']
    API_SEMANTIC_VERSION = "1.0"
    API_PROJECT_NAME = "Project Omie [KYC,ID]"
    API_LAYER = Layer.BACK

    QUEUE_OUTPUT_FILE = DATA.get('QUEUE_OUTPUT_FILE', 'queue-sb-intranet-output-file-dev')
    QUEUE_EXTRACT = DATA.get('QUEUE_EXTRACT', 'queue-sb-intranet-extract-dev')
    QUEUE_KYC_ID = DATA.get('QUEUE_KYC_ID', 'queue-sb-intranet-kyc-id-dev')

    URL_COMPANIES_BIG_DATA = DATA.get('URL_COMPANIES_BIG_DATA')
    ACCESS_TOKEN = DATA.get('ACCESS_TOKEN')

    CELERY_CREATE_MISSING_QUEUES = True
    CELERY_MAX_RETRY_RETRIES = DATA.get('CELERY_MAX_RETRY_RETRIES', 5)
    CELERY_DEFAULT_RETRY_DELAY = DATA.get('CELERY_DEFAULT_RETRY_DELAY', 5)
    CELERY_ACCEPT_CONTENT = ['pickle', 'json', 'msgpack', 'yaml']

    AWS_REGION = DATA.get('AWS_REGION', os.getenv('AWS_REGION', "sa-east-1"))
    CELERY_RESULT_SERIALIZER = 'json'
    CELERY_TASK_SERIALIZER = 'json'
    BROKER_TRANSPORT_OPTIONS = {
        'region': AWS_REGION
    }

    CELERY_QUEUES = {
        QUEUE_OUTPUT_FILE: {
            'exchange': QUEUE_OUTPUT_FILE,
            'binding_key': QUEUE_OUTPUT_FILE,
        },
        QUEUE_EXTRACT: {
            'exchange': QUEUE_EXTRACT,
            'binding_key': QUEUE_EXTRACT,
        },
        QUEUE_KYC_ID: {
            'exchange': QUEUE_KYC_ID,
            'binding_key': QUEUE_KYC_ID,
        },
    }

    BROKER_URL = "sqs://"

    URL_KYC = DATA.get('URL_KYC', 'https://dev-apibackkycscore.smartbank.net.br/v0')
    URL_ID = DATA.get('URL_ID', 'https://dev-apibackidscore.smartbank.net.br/v0')

    PATH_API_STATIC = os.path.join(os.path.dirname(static.__file__), '')
    PATH_LOG = os.path.join(os.path.dirname(reports.__file__), 'logs')
    PATH_FILE = os.path.join(os.path.dirname(files.__file__))

    APP_PORT = DATA.get('PORT', 8080)
    SUBMIT_FORM_TOKEN = DATA.get('SUBMIT_FORM_TOKEN', '441347853E69E92112ACFBC2BE121')
    APP_DEBUG = False


class Developer(Production):
    ENVIRONMENT = 'Developer'
    API_ENVIRONMENT = Env['DEV']
    APP_DEBUG = True

    URL_POST_LOG = 'https://sblog-dev.smartbank.net.br'

    URL_KYC = 'https://dev-apibackkycscore.smartbank.net.br/v0'
    URL_ID = 'https://dev-apibackidscore.smartbank.net.br/v0'

    QUEUE_OUTPUT_FILE = DATA.get('QUEUE_OUTPUT_FILE', 'queue-sb-intranet-output-file-dev')
    QUEUE_EXTRACT = DATA.get('QUEUE_EXTRACT', 'queue-sb-intranet-extract-dev')
    QUEUE_KYC_ID = DATA.get('QUEUE_KYC_ID', 'queue-sb-intranet-kyc-id-dev')

    BROKER_URL = "sqs://"

    CELERY_QUEUES = {
        QUEUE_OUTPUT_FILE: {
            'exchange': QUEUE_OUTPUT_FILE,
            'binding_key': QUEUE_OUTPUT_FILE,
        },
        QUEUE_EXTRACT: {
            'exchange': QUEUE_EXTRACT,
            'binding_key': QUEUE_EXTRACT,
        },
        QUEUE_KYC_ID: {
            'exchange': QUEUE_KYC_ID,
            'binding_key': QUEUE_KYC_ID,
        },
    }


class Homologation(Production):
    ENVIRONMENT = 'Homologation'
    API_ENVIRONMENT = Env['HMG']
    APP_DEBUG = True

    URL_POST_LOG = 'https://sblog-hmg.smartbank.net.br'

    QUEUE_OUTPUT_FILE = DATA.get('QUEUE_OUTPUT_FILE', 'queue-sb-intranet-output-file-hmg')
    QUEUE_EXTRACT = DATA.get('QUEUE_EXTRACT', 'queue-sb-intranet-extract-hmg')
    QUEUE_KYC_ID = DATA.get('QUEUE_KYC_ID', 'queue-sb-intranet-kyc-id-hmg')

    CELERY_QUEUES = {
        QUEUE_OUTPUT_FILE: {
            'exchange': QUEUE_OUTPUT_FILE,
            'binding_key': QUEUE_OUTPUT_FILE,
        },
        QUEUE_EXTRACT: {
            'exchange': QUEUE_EXTRACT,
            'binding_key': QUEUE_EXTRACT,
        },
        QUEUE_KYC_ID: {
            'exchange': QUEUE_KYC_ID,
            'binding_key': QUEUE_KYC_ID,
        },
    }

    BROKER_URL = "sqs://"


class Testing(Production):
    ENVIRONMENT = 'Testing'
    API_ENVIRONMENT = Env['HMG']
    APP_DEBUG = True


class LocalDeveloper(Production):
    ENVIRONMENT = 'LocalDeveloper'
    AWS_ACCESS_KEY_ID = urllib.parse.quote(os.getenv('AWS_ACCESS_KEY_ID', ''), safe='')
    AWS_SECRET_ACCESS_KEY = urllib.parse.quote(os.getenv('AWS_SECRET_ACCESS_KEY', ''), safe='')

    BROKER_URL = "sqs://%s:%s@" % (AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY)

    API_ENVIRONMENT = Env['LOCAL']
    APP_DEBUG = True

    URL_POST_LOG = 'https://sblog-dev.smartbank.net.br'

    URL_COMPANIES_BIG_DATA = "https://bigboost.bigdatacorp.com.br/companies"

    ACCESS_TOKEN = 'ab967eda-ba1b-45fc-b799-362769e347ba'

    URL_KYC = 'http://localhost:8082/v0'
    URL_ID = 'http://localhost:8081/v0'

    SUBMIT_FORM_TOKEN = '441347853E69E92112ACFBC2BE121'

    QUEUE_OUTPUT_FILE = 'queue-sb-intranet-output-file-dev'
    QUEUE_EXTRACT = 'queue-sb-intranet-extract-dev'
    QUEUE_KYC_ID = 'queue-sb-intranet-kyc-id-dev'

    CELERY_QUEUES = {
        QUEUE_OUTPUT_FILE: {
            'exchange': QUEUE_OUTPUT_FILE,
            'binding_key': QUEUE_OUTPUT_FILE,
        },
        QUEUE_EXTRACT: {
            'exchange': QUEUE_EXTRACT,
            'binding_key': QUEUE_EXTRACT,
        },
        QUEUE_KYC_ID: {
            'exchange': QUEUE_KYC_ID,
            'binding_key': QUEUE_KYC_ID,
        },
    }


def _load_config():
    environment = DATA.get('ENVIRONMENT', os.getenv('ENVIRONMENT', 'LocalDeveloper'))
    _config_class = getattr(sys.modules[__name__], str(environment))
    return _config_class()


config = _load_config()
