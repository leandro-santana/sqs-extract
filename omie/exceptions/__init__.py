from sb_pylogger.sb_logger import init_sb_logger, SbLogger

from omie.configs import config


class BaseInitLogger(object):
    init_sb_logger(environment=config.API_ENVIRONMENT,
                   layer=config.API_LAYER,
                   project_name=config.API_PROJECT_NAME,
                   semantic_version=config.API_SEMANTIC_VERSION)

    logger = SbLogger()


class ApplicationError(BaseInitLogger, Exception):
    def __init__(self, message=None):
        self.message = message
        self.logger.log_exception(str(self.message), self.message)


class ValidateRequests(BaseInitLogger, Warning):
    def __init__(self, message=None, success=False):
        self.message = message
        self.success = success
        self.logger.log_info(str(self.message), self.message)
