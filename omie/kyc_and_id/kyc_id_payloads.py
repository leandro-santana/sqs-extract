from omie.utils import Payloads
from omie.configs import config


class KycIdPayloads(object):
    HEADERS = {'token': '123456'}

    def requests_load_kyc_id_pj(self, payload, product_type="ccprepayment", partner_id="123456"):
        payload_kyc = Payloads.get_payload_kyc_pj(payload, product_type=product_type, partner_id=partner_id)
        payload_id = Payloads.get_payload_id_pj(payload, product_type=product_type, partner_id=partner_id)

        send_queue = {'message': {
            'type_check': 'PJ',
            'kyc': {
                'url': '{url}/legalperson'.format(url=config.URL_KYC),
                'payload': payload_kyc,
                'headers': self.HEADERS
            },
            'id': {
                'url': '{url}/legalperson'.format(url=config.URL_ID),
                'payload': payload_id,
                'headers': self.HEADERS
            }
        }}

        return send_queue

    def requests_load_kyc_id_pf(self, payload, product_type="ccprepayment", partner_id="123456"):
        payload_kyc = Payloads.get_payload_kyc_pf(payload, product_type=product_type, partner_id=partner_id)
        payload_id = Payloads.get_payload_id_pf(payload, product_type=product_type, partner_id=partner_id)

        send_queue = {'message': {
            'type_check': 'PF',
            'kyc': {
                'url': '{url}/naturalperson'.format(url=config.URL_KYC),
                'payload': payload_kyc,
                'headers': self.HEADERS
            },
            'id': {
                'url': '{url}/naturalperson'.format(url=config.URL_ID),
                'payload': payload_id,
                'headers': self.HEADERS
            }
        }}
        return send_queue
