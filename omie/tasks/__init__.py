from celery import Celery

from omie.configs import config

app = Celery(include=['omie.tasks.id_kyc'])

app.config_from_object(config)
app.conf.task_queue_max_priority = 10
