import os
import uuid

import requests
from celery import Task
from sb_pylogger.sb_logger import SbLogger, init_sb_logger

from omie import config
from omie.kyc_and_id.kyc_id_payloads import KycIdPayloads
from omie.tasks import app
import pandas as pd
from omie.utils import is_json
from omie.utils.extracts import ExtractCnpj
from omie.utils.log_utils import get_host_log, write_log_response


class ControlTask(Task):
    init_sb_logger(environment=config.API_ENVIRONMENT,
                   layer=config.API_LAYER,
                   project_name=config.API_PROJECT_NAME,
                   semantic_version=config.API_SEMANTIC_VERSION)

    logger = SbLogger()
    url_log = url = get_host_log('omie')

    def after_return(self, status, retval, task_id, args, kwargs, einfo):
        pass


@app.task(base=ControlTask, bind=True)
def worker_job_kyc_id(self, **kwargs):
    try:
        message = kwargs.get('message')
        transaction_id = str(uuid.uuid4())

        response_id = requests.post(url=message.get('id', {}).get('url'),
                                    data=message.get('id', {}).get('payload'),
                                    headers=message.get('id', {}).get('headers'))

        response_kyc = requests.post(url=message.get('kyc', {}).get('url'),
                                     data=message.get('kyc', {}).get('payload'),
                                     headers=message.get('kyc', {}).get('headers'))

        self.logger.log_info('Processed', {
            'type': message.get('type_check'),
            'payload_id': is_json(message.get('id', {}).get('payload')),
            'payload_kyc': is_json(message.get('kyc', {}).get('payload')),
            'response_kyc': {'status': response_kyc.status_code, 'result': is_json(response_kyc.text)},
            'response_id': {'status': response_id.status_code, 'result': is_json(response_id.text)},
        })
        write_log_response(response_output=response_id.text,
                           request_input=message.get('id', {}).get('payload'),
                           url=self.url_log,
                           project="ID Score",
                           transaction_id=transaction_id)

        write_log_response(response_output=response_kyc.text,
                           request_input=message.get('kyc', {}).get('payload'),
                           url=self.url_log,
                           project="KYC Score",
                           transaction_id=transaction_id)

    except Exception as error:
        self.logger.log_exception('Exception', {'error': error, 'message': message})
    finally:
        self.logger.close()


@app.task(base=ControlTask, bind=True)
def worker_job_extract_omie(self, **kwargs):
    script = KycIdPayloads()
    message = kwargs.get("message")
    try:
        payload = script.requests_load_kyc_id_pj(payload=message, product_type="bankslip", partner_id="omie")
        queue_job = worker_job_kyc_id.apply_async(kwargs=payload, queue=config.QUEUE_KYC_ID, priority=1)
        if not queue_job:
            raise Exception('Not Queued')
    except Exception as error:
        self.logger.log_exception('Exception', {'error': error, 'message': message})
    finally:
        self.logger.close()


# omie
@app.task(base=ControlTask, bind=True)
def worker_job_download_omie(self, **kwargs):
    extract_cnpj = ExtractCnpj()
    filename = kwargs.get('filename')
    email = kwargs.get('email', 'email not defined.')
    try:
        path = '{path}/{name_file}'.format(path=config.PATH_FILE, name_file=filename)
        self.logger.log_info('Begin', filename)
        df = pd.read_csv(path, sep=",", encoding='utf-8', keep_default_na=False)

        for index, row in df.iterrows():
            extract_datas = extract_cnpj.extract_relationships(data=row)
            for extract_data in extract_datas:
                queue_job = worker_job_extract_omie.apply_async(kwargs={"message": extract_data},
                                                                queue=config.QUEUE_EXTRACT,
                                                                priority=1)
                if not queue_job.id:
                    raise Exception('Not Queued')
                self.logger.log_info('Queued', {'email': email, 'data': extract_data, })
        os.remove(path)
    except Exception as error:
        self.logger.log_exception('Exception', {'error': error, 'message': kwargs})
    finally:
        self.logger.close()
