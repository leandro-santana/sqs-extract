import os
from tornado import web

from omie.configs import config
from omie.views.healthcheck import HealthcheckApi
from omie.views.front_api import FrontApi

routes = [
    (r'/healthcheck', HealthcheckApi),
    (r'/', FrontApi),
    (r"/static/(.*)", web.StaticFileHandler, {"path": config.PATH_API_STATIC}),
]
