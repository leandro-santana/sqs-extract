import json
import logging
import logging.config
from datetime import datetime


# from omie.configs import config


def clear_none_values_in_dict(obj):
    if isinstance(obj, (list, tuple, set)):
        return type(obj)(clear_none_values_in_dict(x) for x in obj if x is not None and x is not "")
    elif isinstance(obj, dict):
        return type(obj)((clear_none_values_in_dict(k), clear_none_values_in_dict(v))
                         for k, v in obj.items() if (k is not None and v is not None))
    else:
        return obj


def is_json(object_json):
    try:
        return json.loads(object_json)
    except ValueError as e:
        return object_json


class Payloads(object):

    @staticmethod
    def get_payload_kyc_pf(row, product_type="ccprepayment", partner_id="cloudwalk"):
        return json.dumps(clear_none_values_in_dict({
            "naturalPerson": {
                "birthDate": datetime.strptime(row.get('birthday'), '%d/%m/%Y').date().strftime('%Y-%m-%d') if row.get(
                    'birthday') else None,
                "cpf": str('%0*d' % (11, int(row.get('document_number', 0)))),
                "fullName": row.get('legal_name'),
                "address": {
                    "city": row.get('city'),
                    "country": row.get('country', 'Brazil'),
                    "federatedState": row.get('state'),
                }
            },
            "partnerId": partner_id,
            "productType": product_type
        }))

    @staticmethod
    def get_payload_id_pf(row, product_type="ccprepayment", partner_id="cloudwalk"):
        return json.dumps(clear_none_values_in_dict({
            "naturalPerson": {
                "cpf": str('%0*d' % (11, int(row.get('document_number', 0)))),
                "fullName": row.get('legal_name'),
                "sex": row.get('sex'),
                "phoneNumber": row.get('phone_number'),
                "email": row.get('email'),
                "birthDate": datetime.strptime(row.get('birthday'), '%d/%m/%Y').date().strftime('%Y-%m-%d') if row.get(
                    'birthday') else None,
                "birthAddress": {
                    "city": row.get('city'),
                    "country": row.get('country', 'Brazil'),
                    "federatedState": row.get('state'),
                },
                "homeAddress": {
                    "country": row.get('country', 'Brazil'),
                    "federatedState": row.get('state'),
                    "city": row.get('city'),
                    "neighborhood": row.get('neighborhood'),
                    "street": row.get('street'),
                    "streetNumber": str(row.get('number')).strip(),
                    "zipCode": str('%0*d' % (8, row.get('cep', 0))),
                    "complement": row.get('complement', ""),
                },
                "maritalStatus": row.get('maritalStatus'),
                "spouseFullNpayload_idame": row.get('spouseFullName'),
                "motherFullName": row.get('motherFullName'),

                "fatherFullName": row.get('fatherFullName'),
                "nationality": row.get('nationality'),
                "usPerson": False,
                "incomes": 0,
                "assets": 0,
                "quizId": row.get('quizId', 0),
            },
            "partnerId": partner_id,
            "productType": product_type
        }))

    @staticmethod
    def get_payload_kyc_pj(row, product_type="ccprepayment", partner_id="cloudwalk"):
        return json.dumps(clear_none_values_in_dict({
            "legalRepresentative": {
                "cpf": str('%0*d' % (11, int(row.get('cpf', 0)))),
                "fullName": row.get('name'),
                "birthDate": datetime.strptime(row.get('birthday'), '%d/%m/%Y').date().strftime('%Y-%m-%d') if row.get(
                    'birthday') else None,
                "politicallyExposedPerson": False,
                "address": {
                    "country": row.get('country', 'Brazil'),
                    "federatedState": row.get('state'),
                    "city": row.get('city'),
                    "zipCode": str('%0*d' % (8, int(row.get('cep', 0)))),
                }
            },
            "legalPerson": {
                "cnpj": str('%0*d' % (14, int(row.get('document_number', 0)))),
                "economicActivityCode": str('%0*d' % (7, int(row.get('cnae', 0)))),
                "corporateName": row.get('legal_name'),
                "address": {
                    "country": row.get('country', 'Brazil'),
                    "federatedState": row.get('state'),
                    "city": row.get('city'),
                    "zipCode": str('%0*d' % (8, int(row.get('cep', 0)))),
                }
            },
            "productType": product_type,
            "partnerId": partner_id
        }))

    @staticmethod
    def get_payload_id_pj(row, product_type="ccprepayment", partner_id="cloudwalk"):
        return json.dumps(clear_none_values_in_dict({
            "legalRepresentative": {
                "cpf": str('%0*d' % (11, int(row.get('cpf', 0)))),
                "fullName": row.get('name'),
                "birthDate": datetime.strptime(row.get('birthday'), '%d/%m/%Y').date().strftime('%Y-%m-%d') if row.get(
                    'birthday') else None,
                "birthAddress": {
                    "city": row.get('city'),
                    "country": row.get('country', 'Brazil'),
                    "federatedState": row.get('state'),
                },
                "homeAddress": {
                    "country": row.get('country', 'Brazil'),
                    "federatedState": row.get('state'),
                    "city": row.get('city'),
                    "neighborhood": row.get('neighborhood'),
                    "street": row.get('street'),
                    "streetNumber": str(row.get('number')).strip(),
                    "zipCode": str('%0*d' % (8, int(row.get('cep', 0)))),
                    "complement": row.get('complement', ""),
                }
            },
            "legalPerson": {
                "cnpj": str('%0*d' % (14, int(row.get('document_number', 0)))),
                "corporateName": row.get('legal_name'),
                "economicActivityCode": str('%0*d' % (7, int(row.get('cnae', 0)))),
                "address": {
                    "country": row.get('country', 'Brazil'),
                    "federatedState": row.get('state'),
                    "city": row.get('city'),
                    "neighborhood": row.get('neighborhood'),
                    "street": row.get('street'),
                    "streetNumber": str(row.get('number')).strip(),
                    "zipCode": str('%0*d' % (8, int(row.get('cep', 0)))),
                    "complement": row.get('complement', ""),
                }
            },
            "productType": product_type,
            "partnerId": partner_id
        }))
