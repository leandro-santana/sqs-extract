import json
import requests
from omie.configs import config


class ExtractCnpj(object):

    @staticmethod
    def validation_owner(relationship):
        if 'QSA' in relationship.get('RelationshipType'):
            if len(relationship.get('RelatedEntityTaxIdNumber')) == 11:
                return True
        return False

    def extract_relationships(self, data):

        payload = {"AccessToken": config.ACCESS_TOKEN, "Datasets": "relationships",
                   "q": "doc{%i}" % (data.document_number,)}

        response = requests.post(url=config.URL_COMPANIES_BIG_DATA, data=payload)
        data_return = []
        if response.status_code == 200:
            data_relationships = json.loads(response.text)
            relationships = data_relationships.get('Result', {})[0].get('Relationships', {}).get('Relationships', {})

            for relationship in relationships:
                data_write = {
                    'document_number': data.document_number,
                    'legal_name': data.legal_name,
                    'name': '',
                    'cpf': '',
                    'type_relation': ''
                }

                if self.validation_owner(relationship):
                    data_write.update({
                        'name': relationship['RelatedEntityName'],
                        'cpf': relationship['RelatedEntityTaxIdNumber'],
                        'type_relation': relationship['RelationshipType']
                    })
                    data_return.append(data_write)

            return data_return
