import json
from omie.configs import config
from datetime import datetime
import requests


def write_log(type_check, message, response_id, response_kyc, **kwargs):
    add_info = {
        'legal_name': kwargs.get('payload_id', {}).get('legalPerson', {}).get('corporateName'),
        'document_number': kwargs.get('payload_id', {}).get('legalPerson', {}).get('cnpj')
    }
    if type_check == 'PF':
        add_info = {
            'legal_name': kwargs.get('payload_id').get('naturalPerson', {}).get('fullName'),
            'document_number': kwargs.get('payload_id').get('naturalPerson', {}).get('cpf')
        }

    kyc_response = response_kyc.content.decode()
    if is_json(kyc_response):
        kyc_response = json.loads(response_kyc.content.decode())

    id_response = response_id.content.decode()
    if is_json(id_response):
        id_response = json.loads(response_id.content.decode())

    log_sb = {
        "type_check": message.get('type_check'),
        'id_score': {
            'request': json.loads(message.get('id', {}).get('payload')),
            'response': id_response,
            'status_code': response_id.status_code
        },
        'kyc_score': {
            'request': json.loads(message.get('kyc', {}).get('payload')),
            'response': kyc_response,
            'status_code': response_kyc.status_code
        }
    }
    log_sb.update(add_info)

    return log_sb


def is_json(object_json):
    try:
        json.loads(object_json)
    except ValueError as e:
        return False
    return True


def get_host_log(index='omie_kyc_id'):
    hosts = {
        'DEV': "https://sblog-dev.smartbank.net.br",
        'LOCAL': "https://sblog-dev.smartbank.net.br",
        'HMG': "https://sblog-hmg.smartbank.net.br",
        'PRD': "https://prd-sblog.smartbank.net.br",
    }

    if not hosts.get(config.API_ENVIRONMENT.value):
        raise Exception("Invalid environment LOGS!")

    return "{}/{}/_doc/".format(hosts.get(config.API_ENVIRONMENT.value), index)


def write_log_response(response_output, request_input, url, project, transaction_id):
    if type(response_output) is not str:
        raise Exception('format to log error')

    response_to_dict = json.loads(response_output)
    request_to_dict = json.loads(request_input)

    reports = response_to_dict.get('report')

    for report in reports:
        report.update({
            "projectName": project,
            "type": 'Legal Person',
            "message": 'Processed',
            "level": "INFO",
            "loggingLevel": "INFO",
            "timestamp": datetime.utcnow().isoformat(),
            'transaction_id': transaction_id,
            "cnpj": request_to_dict.get('legalPerson', {}).get('cnpj', ''),
            'geral_result': response_to_dict.get('result', ''),
            'geral_restrictive_eval': response_to_dict.get('restrictive_eval', {}),
            'partnerId': request_to_dict.get('partnerId', ''),
            'productType': request_to_dict.get('productType', ''),
            'geral_object_request': request_input,
            'geral_object_response': response_output
        })
        log_response = requests.post(url=url, json=report, verify=False)

        if log_response.status_code not in [200, 201]:
            raise Exception('Log Fail')
    return True

