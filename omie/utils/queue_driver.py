from omie.tasks.id_kyc import worker_job_kyc_id, worker_job_extract_omie, worker_job_download_omie
from omie.utils.singleton import Singleton


class QueueDriver(metaclass=Singleton):
    list_jobs_queues = {
        'worker_job_kyc_id': worker_job_kyc_id,
        'worker_job_extract_omie': worker_job_extract_omie,
        'worker_job_download_omie': worker_job_download_omie
    }

    def __init__(self, job_queue):
        job = self.list_jobs_queues.get(job_queue)
        if not job:
            raise Exception('has not queue {queue_job}'.format(queue_job=job_queue))

        self.__job_queue = self.list_jobs_queues.get(job_queue)

    def send_to_queue(self, message, **kwargs):
        queue_id = self.__job_queue.apply_async(
            kwargs=message, queue=kwargs.get('queue'),
            priority=int(kwargs.get('priority')))
        return queue_id
