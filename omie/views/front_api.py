import os

import requests
from sb_pylogger.sb_logger import init_sb_logger, SbLogger
from tornado.web import RequestHandler

from omie import config
from omie.exceptions import ValidateRequests, ApplicationError
from omie.kyc_and_id import files
from omie.utils.log_utils import get_host_log
from omie.utils.queue_driver import QueueDriver
from datetime import datetime


class FrontApi(RequestHandler):
    PATH_FILE = os.path.join(os.path.dirname(files.__file__))
    app_title = "Omie Api"
    app_template = "templates/front_api/index.html"

    init_sb_logger(environment=config.API_ENVIRONMENT,
                   layer=config.API_LAYER,
                   project_name=config.API_PROJECT_NAME,
                   semantic_version=config.API_SEMANTIC_VERSION)

    logger = SbLogger()

    def get(self):
        self.render(self.app_template, title=self.app_title, message='',
                    environment=config.ENVIRONMENT, success=False)

    def post(self):
        try:
            token = self.request.body_arguments['acess_token'][0]
            email = self.request.body_arguments['email'][0].decode()
            email_domain = email.split('@')[1]

            if email_domain not in ['smartbank.com.br']:
                raise ValidateRequests(message='Email is not smartbank')

            if config.SUBMIT_FORM_TOKEN in [token.decode()]:
                file = self.request.files['document'][0]

                if file.content_type not in ['text/csv']:
                    raise ValidateRequests(message='File is not csv extension.')

                filename = '{filename}.csv'.format(filename='data')
                output_file = open('{path}/{file}'.format(path=self.PATH_FILE, file=filename), 'wb')
                output_file.write(file['body'])

                queue_driver = QueueDriver('worker_job_download_omie')
                queue_id = queue_driver.send_to_queue(message={
                    'filename': filename, 'email': email},
                    queue=config.QUEUE_OUTPUT_FILE, priority=1)
                if queue_id:
                    raise ValidateRequests(message='Sended Data.', success=True)
            else:
                raise ValidateRequests(message='Invalid Token.')
        except ValidateRequests as validate:
            self.render(self.app_template, title=self.app_title, message=str(validate.message),
                        success=validate.success, environment=config.ENVIRONMENT)
        except ApplicationError as error:
            self.render(self.app_template, title=self.app_title, message=str(error),
                        environment=config.ENVIRONMENT)
        except Exception as error:
            self.logger.log_exception('Exception', error)
            self.render(self.app_template, title=self.app_title, message=str(error),
                        success=False, environment=config.ENVIRONMENT)
