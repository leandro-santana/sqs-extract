import requests
from healthcheck import HealthCheck
from kombu import Connection
from omie.configs import config
from omie.views import ApiJsonHandler


class HealthcheckApi(ApiJsonHandler):
    @staticmethod
    def __check_queue():
        broker_url = config.BROKER_URL
        connection = Connection(broker_url)
        connection.ensure_connection(max_retries=1)
        return True, "queue ok"

    @staticmethod
    def __check_url_id_score():
        response = requests.get(url='{url}/health'.format(url=config.URL_ID))

        if response.status_code == 200:
            return True, "url_kyc_score ok"
        return False, "url_kyc_score is not ok"

    @staticmethod
    def __check_url_kyc_score():
        response = requests.get(url='{url}/health'.format(url=config.URL_KYC))

        if response.status_code == 200:
            return True, "url_kyc_score ok"
        return False, "url_kyc_score is not ok"

    def get(self, *args, **kwargs):
        health = HealthCheck()

        health.add_check(self.__check_queue)
        health.add_check(self.__check_url_id_score)
        health.add_check(self.__check_url_kyc_score)

        message, status_code, headers = health.run()
        self.set_status(status_code)
        self.finish(message)
